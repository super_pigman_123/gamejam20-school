//C Standard Library Modules
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
//Imported Modules
#include "raylib.h" //Do not change
//Local Modules
#include "parameters.h"
#include "room_logic.h"
#include "main.h"
#include "endConditions.h"
#include "endGameFunctions.h"
#include "jukefunctions.h"

//Global variables that will be changed throughout the course of the program. 
//Add all gameplay variables here.
const int MAX_NUMBER_OF_ROOMS = 36; 
const int MAX_NUMBER_OF_STUDENTS = 200;
const int SQUARE_BUTTON_SIZE = 100;
//--------------GLOBAL-----------------
int playerMoneyValue = 500;
int playerStudentsValue = 0;
int playerStudentCapacity = 0;
int playerNumberRooms = 0;
int playerStudentHappiness = 500;
int playerPrestige = 0;
int updateTracker = 10;
int tutorialTrack = 0;
struct room * selectedRoom;
struct roomMap room_map;
Camera2D camera = {0};
//--------------Increments------------
int moneyOverTime = 1;
int studentsOverTime = 1;
int happinessOverTime = -10;

//-------------BUTTON_BOUNDS----------
Rectangle plusRoom;
Rectangle rmvRoom;
Rectangle upRoom;
Rectangle ext;
//-------------------------------------

/*
 *  Function designed to (1) set initial room
 *  (2) set initial properties; e.g. number of students
 */
void init_map() {
    playerMoneyValue += classroom.level_cost[0];
    buyRoom(&room_map.map[3][3],&classroom);

    playerStudentsValue = playerStudentCapacity;

    srand(time(NULL));
    for (int i=0; i<map_maxx; i++) {
        for (int y=0;y<map_maxy; y++) {
            if (room_map.map[i][y].currentLevel == 0) {
                room_map.map[i][y].texture_type = rand()%3;
            }
        }
    }
}

/*
 * Draws a room from a room_map location
 * (!) roomMap must be initialised
 */
int DrawRoomFromMap(int blockx, int blocky) {
    struct room * r = &room_map.map[blockx][blocky];
    Texture2D drawable;
    srand(time(NULL)); //init rand()

    if (blockx >= 0 && blockx<map_maxx && blocky >= 0 && blocky<map_maxy) {

        //select room type
        if (r->currentLevel == 0) {
            drawable = empty[r->texture_type];

        } else if(r->currentLevel>0) {
            if(r->type == &sportsHall) {
                drawable = roomT_SportsHall;
            }
            if(r->type == &cafe) {
                drawable = roomT_Cafe;
            }
            if(r->type == &classroom) {
                drawable = roomT_Class;
            }
            if(r->type == &library) {
                drawable = roomT_Library;
            }
            if(r->type == &scienceLab) {
                drawable = roomT_ScienceLab;
            }
        }

        //draw the room
        if (selectedRoom == r) {
            DrawTexture(drawable, BlockX_to_AbsX(blockx), BlockY_to_AbsY(blocky), SKYBLUE);

        } else {
            DrawTexture(drawable, BlockX_to_AbsX(blockx), BlockY_to_AbsY(blocky), WHITE);
        }

    } else {
        return -1;
    }
}

int DrawKidsInRooms() {
    int kids_to_draw;
    if (playerNumberRooms>0) {
        kids_to_draw = playerStudentsValue / playerNumberRooms;
    } else {
        return -1;
    }

    for (int i=0; i<map_maxx; i++) {
        for (int y=0;y<map_maxy; y++) {
            if (room_map.map[i][y].currentLevel != 0) {
                for(int k = 0;k<kids_to_draw;k++) {
                    DrawTexture(schoolguys[rand() % 4], BlockX_to_AbsX(i) + room_map.block_size/5 + rand() % room_map.block_size*0.3 ,
                                BlockY_to_AbsY(y) + room_map.block_size/5 + rand() % room_map.block_size*0.3, WHITE);
                }
            }
        }
    }

    return 0;
}

/*
 * Draws the whole map from room_map structure
 */
void DrawFullRoomMap() {
    for (int i=0; i<map_maxx; i++) {
        for (int y=0;y<map_maxy; y++) {
            if (room_map.map[i][y].currentLevel > 0) {
                DrawRoomFromMap(i,y);
            } else {
                DrawRoomFromMap(i,y);
            }
        }
    }
}

void initRoomRectangles() {
    for (int i=0; i<map_maxx; i++) {
        for (int y=0;y<map_maxy; y++) {
            room_map.map[i][y].clickable = makeRectangle(BlockX_to_AbsX(i),BlockY_to_AbsY(y),room_map.block_size,room_map.block_size);
        }
    }
}

Rectangle makeRectangle (int x, int y, int w, int h) {
    Rectangle returnable = {x,y,w,h};
    return returnable;
}

void processMapClicks() {
    Vector2 mousePoint = GetMousePosition();

    if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
        if(getRoomPointedByMouse(mousePoint) == NULL) selectedRoom=selectedRoom;
        else(selectedRoom = getRoomPointedByMouse(mousePoint));
    }
}

struct room * getRoomPointedByMouse(Vector2 mouse_pos) {

    for (int i = 0; i<map_maxx;i++) {
        for (int y = 0;y<map_maxy;y++) {

            Vector2 rec_top_left_point = {room_map.map[i][y].clickable.x, room_map.map[i][y].clickable.y};
            Vector2 rec_bottom_right_point = {room_map.map[i][y].clickable.x + room_map.block_size,
                                              room_map.map[i][y].clickable.y + room_map.block_size};

            Vector2 rel_rec_top_left_point = GetWorldToScreen2D(rec_top_left_point,camera);
            Vector2 rel_rec_bottom_right_point = GetWorldToScreen2D(rec_bottom_right_point,camera);

            if (mouse_pos.x>rel_rec_top_left_point.x && mouse_pos.y > rel_rec_top_left_point.y &&
                mouse_pos.x<rel_rec_bottom_right_point.x && mouse_pos.y<rel_rec_bottom_right_point.y) {

                return &room_map.map[i][y];

            }

        }
    }
    
    return NULL;
}


int BlockX_to_AbsX(int x) {
    return room_map.grid_x + room_map.block_size*x;
}
int BlockY_to_AbsY(int y) {
    return room_map.grid_y + room_map.block_size*y;
}

int main(){

    //Initialisations for window, do not change. Target: 720p.
    const int screenWidth = 1080;
    const int screenHeight = 720;
    InitWindow(screenWidth, screenHeight, "Village School - Game Jam 2020");
    SetTargetFPS(60);

    InitAudioDevice(); //Required for audio, don't change.
    SetMasterVolume(10);

    //Initialise logical structures
    initRoomMapStructure(100,100,200);

    initialiseRoomTypes();

    //Initialise any UI textures here
    //TOP MENU
    Texture2D money = LoadTexture("img/money.png"); 
        money.width = 80;
        money.height = 80;
    Texture2D person = LoadTexture("img/person_1.png");
        person.width = 80; 
        person.height = 80;
    Texture2D building = LoadTexture("img/building.png");
        building.width = 80;
        building.height = 80;
    Texture2D happiness = LoadTexture("img/happiness.png");
        happiness.width = 80;
        happiness.height = 80;
    Texture2D prestige = LoadTexture("img/prestige.png");
        prestige.width = 80;
        prestige.height = 80;
    //RIGHT MENU
    Texture2D addRoom = LoadTexture("img/buttonADD.png");
        addRoom.width = SQUARE_BUTTON_SIZE;
        addRoom.height = SQUARE_BUTTON_SIZE;
    Texture2D placeholderButton = LoadTexture("img/button placeholder.png");
        placeholderButton.width = SQUARE_BUTTON_SIZE;
        placeholderButton.height = SQUARE_BUTTON_SIZE;
    Texture2D removeRoom = LoadTexture("img/buttonRemove.png");
        removeRoom.width = SQUARE_BUTTON_SIZE;
        removeRoom.height = SQUARE_BUTTON_SIZE;
    Texture2D upgradeRoomT = LoadTexture("img/buttonUpgrade.png");
        upgradeRoomT.width = SQUARE_BUTTON_SIZE;
        upgradeRoomT.height = SQUARE_BUTTON_SIZE;
    Texture2D exitGame = LoadTexture("img/buttonClose.png");
        exitGame.width = SQUARE_BUTTON_SIZE;
        exitGame.height = SQUARE_BUTTON_SIZE;

    //Initialise any room textures here
    empty[0] = LoadTexture("img/rubble.png");
        empty[0].width = room_map.block_size;
        empty[0].height = room_map.block_size;
    empty[1] = LoadTexture("img/grass.png");
        empty[1].width = room_map.block_size;
        empty[1].height = room_map.block_size;
    empty[2] = LoadTexture("img/trash.png");
        empty[2].width = room_map.block_size;
        empty[2].height = room_map.block_size;
    roomT_SportsHall = LoadTexture("img/sportshall.png");
        roomT_SportsHall.width = room_map.block_size;
        roomT_SportsHall.height = room_map.block_size;
    roomT_Cafe = LoadTexture("img/cafe.png");
        roomT_Cafe.width = room_map.block_size;
        roomT_Cafe.height = room_map.block_size;
    roomT_Class = LoadTexture("img/classroom.png");
        roomT_Class.width = room_map.block_size;
        roomT_Class.height = room_map.block_size;
    roomT_ScienceLab = LoadTexture("img/sciencelab2.png");
        roomT_ScienceLab.width = room_map.block_size;
        roomT_ScienceLab.height = room_map.block_size;
    roomT_Library = LoadTexture("img/library.png");
        roomT_Library.width = room_map.block_size;
        roomT_Library.height = room_map.block_size;

    //Initialise any other textures here
    const float schoolguy_multiplier = 0.2;
    schoolguys[0] = LoadTexture("img/person_1.png");
        schoolguys[0].width = room_map.block_size * schoolguy_multiplier;
        schoolguys[0].height = room_map.block_size * schoolguy_multiplier;
    schoolguys[1] = LoadTexture("img/person_2.png");
        schoolguys[1].width = room_map.block_size * schoolguy_multiplier;
        schoolguys[1].height = room_map.block_size * schoolguy_multiplier;
    schoolguys[2] = LoadTexture("img/person_3.png");
        schoolguys[2].width = room_map.block_size * schoolguy_multiplier;
        schoolguys[2].height = room_map.block_size * schoolguy_multiplier;
    schoolguys[3] = LoadTexture("img/person_4.png");
        schoolguys[3].width = room_map.block_size * schoolguy_multiplier;
        schoolguys[3].height = room_map.block_size * schoolguy_multiplier;
    Texture2D oldMan = LoadTexture("img/oldguy.png");

    //Initialise any audio tracks here
    Music MainTheme = LoadMusicStream("sound/Soundtrack-11.ogg");
    Music LosingTheme = LoadMusicStream("sound/Soundtrack-12.ogg");
    Music currentlyPlaying = MainTheme;
    int musicSwitch = 0;
    PlayMusicStream(currentlyPlaying);

    //Initialise camera here.
    camera.zoom = 1.00f;
    camera.target = (Vector2){BlockX_to_AbsX(1),BlockY_to_AbsY(1)};

    //INITIAL MAP SETUP
    init_map();
    initRoomRectangles();
    //debug_mapToSportshall();

    bool showBuyMenu = false;

    while (!WindowShouldClose())
    {
    BeginDrawing();
            //Most game functionality will exist inside this funcition.
            ClearBackground(BLACK); //ALWAYS RESET BACKGROUND
            UpdateMusicStream(currentlyPlaying); //Required to keep music playing.
            double gameTime = GetTime(); //Timekeeping variable

            //Music switching functions. Will automatically switch depending on the happiness level.
            if (playerStudentHappiness <= 400 && musicSwitch == 0){
                StopMusicStream(currentlyPlaying);
                currentlyPlaying = LosingTheme;
                PlayMusicStream(currentlyPlaying);
                musicSwitch = 1;
            }
            if (playerStudentHappiness > 400 && musicSwitch ==1){
                StopMusicStream(currentlyPlaying);
                currentlyPlaying = MainTheme;
                PlayMusicStream(currentlyPlaying);
                musicSwitch = 0;
            }

            //Test keys for switching audio. Delete before release build.
            /* if (IsKeyPressed(KEY_P))
                playerStudentHappiness = 0;
            if (IsKeyPressed(KEY_O))
                playerStudentHappiness = 700; */

            //Camera mode initialisation. 
            BeginMode2D(camera);
            if(!showBuyMenu) processMapClicks();
            checkCapacity();
            DrawFullRoomMap();
            DrawKidsInRooms();
            EndMode2D();

            //CODEBASE FOR KEY INPUT
            if(IsKeyDown(KEY_LEFT)) camera.offset.x += 10;
            if(IsKeyDown(KEY_RIGHT)) camera.offset.x -= 10;
            if(IsKeyDown(KEY_UP)) camera.offset.y += 10;
            if(IsKeyDown(KEY_DOWN)) camera.offset.y -= 10;

            //CODEBASE FOR CAMERA LIMITS
            //Currently commented out for macOS compatability. Uncomment before release.
            if(camera.offset.x < -100) camera.offset.x=-100;
            if(camera.offset.x > 850) camera.offset.x=850;
            if(camera.offset.y < -350) camera.offset.y=-350;
            if(camera.offset.y > 600) camera.offset.y=600;

            //Camera Zoom Codebase
            if(IsKeyDown(KEY_MINUS) && camera.zoom>0.8f){
                camera.zoom -= 0.01f;
            }
            if(IsKeyDown(KEY_EQUAL) && camera.zoom<2.0f){
                camera.zoom += 0.01f;
            }

            //CODEBASE FOR DRAWING RIGHT MENU---------(Note, this must stay above the top menu draw code)
            DrawRectangle(980, -1, 101, 730, GRAY);
            DrawTexture(addRoom, 980, 100, WHITE);
                plusRoom = makeRectangle(980,100, SQUARE_BUTTON_SIZE,SQUARE_BUTTON_SIZE);
            DrawTexture(removeRoom, 980, 200, WHITE);
                rmvRoom = makeRectangle(980,200, SQUARE_BUTTON_SIZE,SQUARE_BUTTON_SIZE);
            DrawTexture(upgradeRoomT, 980, 300, WHITE);
                upRoom = makeRectangle(980,300, SQUARE_BUTTON_SIZE,SQUARE_BUTTON_SIZE);
            //DrawTexture(exitGame, 980, 400, WHITE);
            DrawText("?",1015,660,50,YELLOW);

            if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && CheckCollisionPointRec(GetMousePosition(),rmvRoom) && selectedRoom!=NULL) {
                remRoom(selectedRoom);
            }

            Rectangle buyMenu = {700,100, 280,250};
            if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && CheckCollisionPointRec(GetMousePosition(),plusRoom) && selectedRoom!=NULL) {
                if(selectedRoom->currentLevel==0)   showBuyMenu = true;
            } else if(showBuyMenu && !CheckCollisionPointRec(GetMousePosition(),plusRoom) && !CheckCollisionPointRec(GetMousePosition(),buyMenu)) {
                showBuyMenu=false;
            }
            if(showBuyMenu){
                DrawRectangleRec(buyMenu, LIGHTGRAY);
                Vector2 mp = GetMousePosition();
                Rectangle highlight1 = {700,165,280,30};
                Rectangle highlight2 = {700,195,280,30};
                Rectangle highlight3 = {700,225,280,30};
                Rectangle highlight4 = {700,255,280,30};
                Rectangle highlight5 = {700,285,280,30};
                if(CheckCollisionPointRec(mp, highlight1)) DrawRectangleRec(highlight1, DARKGRAY);
                if(CheckCollisionPointRec(mp, highlight2)) DrawRectangleRec(highlight2, DARKGRAY);
                if(CheckCollisionPointRec(mp, highlight3)) DrawRectangleRec(highlight3, DARKGRAY);
                if(CheckCollisionPointRec(mp, highlight4)) DrawRectangleRec(highlight4, DARKGRAY);
                if(CheckCollisionPointRec(mp, highlight5)) DrawRectangleRec(highlight5, DARKGRAY);

                if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                    if (CheckCollisionPointRec(mp, highlight1)) buyRoom(selectedRoom, &classroom);
                    else if(CheckCollisionPointRec(mp,highlight2)) buyRoom(selectedRoom, &sportsHall);
                    else if(CheckCollisionPointRec(mp,highlight3)) buyRoom(selectedRoom, &scienceLab);
                    else if(CheckCollisionPointRec(mp,highlight4)) buyRoom(selectedRoom, &library);
                    else if(CheckCollisionPointRec(mp,highlight5)) buyRoom(selectedRoom, &cafe);
                }


                DrawText("Choose room to build:",710,110,20,GRAY);
                DrawText(FormatText("Classroom - Cost %i",150),710,170,20,GRAY); // CHANGE PRICES LATER
                DrawText(FormatText("Sports Hall - Cost %i",500),710,200,20,GRAY); // CHANGE PRICES LATER
                DrawText(FormatText("Science Lab - Cost %i",1000),710,230,20,GRAY); // CHANGE PRICES LATER
                DrawText(FormatText("Library - Cost %i",200),710,260,20,GRAY); // CHANGE PRICES LATER
                DrawText(FormatText("Cafeteria - Cost %i",300),710,290,20,GRAY); // CHANGE PRICES LATER

            }
            if(selectedRoom!=NULL) {
                if (selectedRoom->currentLevel != 0 && IsMouseButtonPressed(MOUSE_LEFT_BUTTON) &&
                    CheckCollisionPointRec(GetMousePosition(), upRoom))
                    upgradeRoom(selectedRoom);
            }
            bool showHelp = false;
            if(CheckCollisionPointRec(GetMousePosition(),(Rectangle) {1000,650,60,60})) showHelp = true;
            if(showHelp){
                DrawRectangle(700,600,280,200,LIGHTGRAY);
                DrawText("Arrow keys: move camera",710,610,20,GRAY);
                DrawText("-/=: Zoom in/out",710,640,20,GRAY);
                DrawText("Click on rooms to\nbuild/upgrade",710,670,20,GRAY);
            }
                ext = makeRectangle(980,400, SQUARE_BUTTON_SIZE,SQUARE_BUTTON_SIZE);
            //----------------------------------------

            //CODEBASE FOR DRAWING TOP MENU-----------
            DrawRectangle(-1, -1, 1085, 101, GRAY); 
            DrawTexture(money, 10, 10, WHITE);
            DrawText(FormatText("%04i", playerMoneyValue), 110, 40, 30, LIGHTGRAY); //Takes money value from global variables above
            DrawTexture(person, 260, 10, WHITE);
            DrawText(FormatText("%04i", playerStudentsValue), 360, 40, 30, LIGHTGRAY); //Takes person value ""
            DrawTexture(building, 510, 10, WHITE); 
            DrawText(FormatText("%04i", playerNumberRooms), 610, 40, 30, LIGHTGRAY); //Takes number of rooms value
            DrawTexture(happiness, 760, 10, WHITE);
            DrawText(FormatText("%04s", happiness_to_percentage(playerStudentHappiness)), 860, 40, 30, LIGHTGRAY); //Takes happiness value
            DrawTexture(prestige, 980, 10, WHITE);
            DrawText(FormatText("%01i", playerPrestige), 1012, 40, 30, BLACK);
            //----------------------------------------

            //CODEBASE FOR TUTORIAL MESSAGE
            if (tutorialTrack == 0){
                DrawRectangle(290, 290, 470, 220, BLACK);
                DrawRectangle(300, 300, 450, 200, LIGHTGRAY);
                DrawTexture(oldMan, 310, 340, WHITE);
                DrawText("Thank you for accepting this contract", 430, 340, 15, BLACK);
                DrawText(" to repair the school. I am allowing you", 430, 360, 15, BLACK);
                DrawText(" full control over the construction,", 430, 380, 15, BLACK);
                DrawText(" demolition, and upgrading of this building.", 430, 400, 15, BLACK);
                DrawText(" Please do your best to return this", 430, 420, 15, BLACK);
                DrawText(" establishment to it's former glory.", 430, 440, 15, BLACK);
                DrawText(" Press SPACE to remove this message...", 430, 470, 15, BLACK);
                if (IsKeyPressed(KEY_SPACE))
                    tutorialTrack = 1;
            }
            //------------------------      

            //Timekeeping functionality. Tracks game state. Runs each loop, updates every 5 seconds. Don't change.
            if(floor(gameTime) == updateTracker){
                updatePlayerParameters();
                if(checkGameState() == 1 || checkGameState() == -1){
                    if (checkGameState() == 1) gameWon();
                    if (checkGameState() == -1) gameLost();
                }
                updateTracker = updateTracker + 5;
            }
            //---------------------------------------

            //WORK ABOVE THIS LINE-------------      
            DrawText("Alpha v0.01", 3, 700, 20, BLACK); //Keep track of version, please edit as required.
        EndDrawing();
    }

    //Textures automatically unload upon close, so no need to unitialise most values.
    //This is the generic closing procedure. Doesn't need to be changed.
    CloseWindow();
    return 0;
}