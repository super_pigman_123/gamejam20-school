#ifndef TEST1_ROOM_LOGIC_H
#define TEST1_ROOM_LOGIC_H

#include <raylib.h>
#include <string.h>
#include "global_variables.h"

extern const int map_maxx;
extern const int map_maxy;

struct roomType {
    int capacity[5];
    int level_cost[5];
    int happiness_added;
    int money_added[5]; //Per 10seconds
};

struct roomType classroom;
struct roomType cafe;
struct roomType library;
struct roomType sportsHall;
struct roomType scienceLab;

struct room {
    struct roomType *type;
    int currentLevel;
    Rectangle clickable;
    int texture_type; //used only for roomtypes which have several textures!
};

struct roomMap {
    struct room map[6][6];
    int grid_x;
    int grid_y;
    int block_size;
};

//Room textures
Texture2D empty[3];
Texture2D roomT_SportsHall;
Texture2D roomT_Cafe;
Texture2D roomT_Class;
Texture2D roomT_ScienceLab;
Texture2D roomT_Library;

//People textures
Texture2D schoolguys[4];

//room ops
int initRoomMapStructure(int location_x, int location_y, int block_size);
int upgradeRoom(struct room * room);
int buyRoom(struct room * room_to_buy, struct roomType *type_to_set);
int remRoom(struct room * room_to_remove);
int initialiseRoomTypes();
void checkCapacity();

//debug
void debug_mapToSportshall();
void debug_putSportsHallIntoMap(int x, int y);

#endif //TEST1_ROOM_LOGIC_H
