#include "parameters.h"

int canRoombeAdded(int x, int y){
    if ((x + y) > MAX_NUMBER_OF_ROOMS)
        return 0;
    return 1;
}

int canStudentBeAdded(int x, int y){
    if ((x + y) > MAX_NUMBER_OF_STUDENTS || (x + y) > playerStudentCapacity)
        return 0;
    return 1;
}

void flatAddMoney(int x){
    playerMoneyValue = playerMoneyValue + x;
}

void flatAddStudents(int x){
    if (canStudentBeAdded(playerStudentsValue, x) == 1)
        playerStudentsValue = playerStudentsValue + x;
}

void flatIncreaseStudentCapacity(int x){
    playerStudentCapacity = playerStudentCapacity + x;
}

void flatAddHappiness(int x){
    playerStudentHappiness = playerStudentHappiness + x;
}

void flatRemoveMoney(int x){
    playerMoneyValue = playerMoneyValue - x;
}

void flatRemoveStudents(int x){
    playerStudentsValue = playerStudentsValue - x;
}

void flatDecreaseStudentCapacity(int x){
    playerStudentCapacity = playerStudentCapacity - x;
}

void flatRemoveHappiness(int x){
    playerStudentHappiness = playerStudentHappiness - x;
}

void addBuildingParam(){
    if (canRoombeAdded(playerNumberRooms, 1) == 1)
        playerNumberRooms++;
}

void removeBuildingParam(){
    playerNumberRooms--;
}

void increaseMoneyOverTime(int x){
    moneyOverTime = moneyOverTime + x;
}

void decreaseMoneyOverTime(int x){
    moneyOverTime = moneyOverTime - x;
}

void increaseStudentsOverTime(int x){
    studentsOverTime = studentsOverTime + x;
}

void decreaseStudentsOverTime(int x){
    studentsOverTime = studentsOverTime - x;
}

void increaseHappinessOverTime(int x){
    happinessOverTime = happinessOverTime + x;
}

void decreaseHappinessOverTime(int x){
    happinessOverTime = happinessOverTime - x;
}

void updatePlayerParameters(){
    flatAddMoney(moneyOverTime);
    flatAddStudents(studentsOverTime);
    flatAddHappiness(happinessOverTime);
}

