#include "raylib.h"

void gameLost(){
    while (!WindowShouldClose()){
        BeginDrawing();
        DrawRectangle(-1, -1, 2000, 2000, LIGHTGRAY);
        DrawText("Dear Principal,", 100, 150, 20, BLACK);
        DrawText("The School Repair Project has unfortunatley been terminated.", 100, 200, 20, BLACK);
        DrawText("All assets have been liquidated, and the building will be demolished in the coming weeks.", 100, 250, 20, BLACK);
        DrawText("This is your official notice of dismissal, effective immediately.", 100, 300, 20, BLACK);
        DrawText("The council appreciates your efforts.", 100, 350, 20, BLACK);
        DrawText("Please click the mouse to exit the game...", 100, 400, 20, BLACK);       
        DrawText("Thank you for playing!!", 250, 500, 50, YELLOW);
        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
            exit(0);
        EndDrawing();
    }
}

void gameWon(){
    while (!WindowShouldClose()){
        BeginDrawing();
        DrawRectangle(-1, -1, 2000, 2000, LIGHTGRAY);
        DrawText("Congratulations!", 100, 150, 20, BLACK);
        DrawText("The school has become one of the top ranking in the country!", 100, 200, 20, BLACK);
        DrawText("The council has deemed the School Repair Project complete.", 100, 250, 20, BLACK);
        DrawText("As thanks, a statue in your honour has been erected outside the school.", 100, 300, 20, BLACK);
        DrawText("Please click the mouse to exit the game...", 100, 350, 20, BLACK);
        DrawText("Thank you for playing!!", 250, 500, 50, YELLOW);
        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
            exit(0);
        EndDrawing();
    }
}