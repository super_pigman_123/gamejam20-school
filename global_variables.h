#ifndef GLOBAL_VARIABLES_H
#define GLOBAL_VARIABLES_H

#include "room_logic.h"

//Global variables that will be changed throughout the course of the program.
extern int playerMoneyValue;
extern int playerStudentsValue;
extern int playerNumberRooms;
extern int playerStudentHappiness;
extern struct roomMap room_map;
extern struct room * selectedRoom;

#endif //GLOBAL_VARIABLES_H