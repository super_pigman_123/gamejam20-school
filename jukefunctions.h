//Friendly reminder to ignore compiler errors from this file. Clang doesn't play nice with NULL,
//but it works fine at run time, so do not change.
//takes happiness (value from 0 to 1000) and returns pointer to string displaying value as a percentage
char* happiness_to_percentage(int happiness)
{
    char* percentage=(char*) malloc(7*sizeof(char));
    char reverse[5];
    int digits=0;
    while(happiness)
    {
        reverse[digits]=happiness%10+'0';
        happiness/=10;
        digits++;
    }
    if(digits==0)
    {
        percentage[0]='0';
        percentage[1]='%';
        percentage[2]= NULL;
    }
    if(digits==1)
    {
        reverse[digits]='0';
        digits++;
    }
    int i;
    int k=0;
    for(i=digits-1;i>=0;i--)
        if(i!=0)
            {
                percentage[k]=reverse[i];
                k++;
            }
        else
        {
            if(reverse[i]!='0')
            {
                percentage[k]='.';
                k++;
                percentage[k]=reverse[i];
                k++;
            }
            percentage[k]='%';
            k++;
            percentage[k]= NULL;
        }

    return percentage;
}
