#include "room_logic.h"
#include "parameters.h"
#include <stdio.h>

const int map_maxx = 6;
const int map_maxy = 6;

/* Room struct example:
 *
 * room.type = struct "Classroom",capacity=[10,15,20,25,30],
 *      moneyadded=[1,2,3,4,5], happinessadded=[2,3,4,5,6], levelcost=[10,20,30,40,50]
 * currentlevel = 2
 * id = 5 (5th element in grid)
 *
 */

int initRoomMapStructure(int location_x, int location_y, int block_size) {
    if (location_x>0 && location_y>0 && block_size>0) {
        room_map.grid_x = location_x;
        room_map.grid_y = location_y;
        room_map.block_size = block_size;
        return 0;
    } else {
        return -1;
    }
}

int upgradeRoom(struct room * room){
    if(playerMoneyValue < room->type->level_cost[room->currentLevel-1] || room->currentLevel==5) return -1;
    playerMoneyValue -=room->type->level_cost[room->currentLevel-1];
    room->currentLevel++;
    increaseMoneyOverTime(room->type->money_added[room->currentLevel-1]);
    increaseHappinessOverTime(room->type->happiness_added);
    if(room->currentLevel == 5) playerPrestige++;
    return 0;
}

/*
 *  Expected use :
 *  When user selects some empty (rubble) space, then selects what they want to buy there,
 *  this function is called.
 */
int buyRoom(struct room * room_to_buy, struct roomType *type_to_set){

    if(room_to_buy->currentLevel!=0) return -1;
    else {
        if(playerMoneyValue<type_to_set->level_cost[0]) {
            return -1;
        } else {

            room_to_buy->type=type_to_set;
            room_to_buy->currentLevel=1;
            playerMoneyValue -= type_to_set->level_cost[0];
            ++playerNumberRooms;
            moneyOverTime += type_to_set->money_added[0];
            playerStudentCapacity += type_to_set->capacity[0];
            playerStudentHappiness+=type_to_set->happiness_added;
            increaseMoneyOverTime(type_to_set->money_added[0]);
            playerStudentHappiness+=type_to_set->happiness_added;
            increaseMoneyOverTime(type_to_set->money_added[0]);


            return 0;
        }
    }
    return 1;
}

int remRoom(struct room * room_to_remove) {
    if (room_to_remove->currentLevel!=0) {
        room_to_remove->currentLevel=0;
        flatDecreaseStudentCapacity(room_to_remove->type->capacity[room_to_remove->currentLevel]);
        playerNumberRooms-=1;
        decreaseMoneyOverTime(room_to_remove->type->money_added[room_to_remove->currentLevel]);
    } else {
        return -1;
    }
    return 1;
}

void checkCapacity() {
    if(playerStudentCapacity<playerStudentsValue) {
        playerStudentsValue = playerStudentCapacity;
    }
}

void debug_mapToSportshall(){
    for (int i=0; i<map_maxx; i++) {
        for (int y=0;y<map_maxy; y++) {
            debug_putSportsHallIntoMap(i,y);
        }
    }
}

void debug_putSportsHallIntoMap(int x, int y) {
    struct roomType t;
    struct room room;
    room.type = &t;
    room_map.map[x][y] = room;
}

int initialiseRoomTypes(){
    classroom.level_cost[0] = 150;
    classroom.level_cost[1] = 150;
    classroom.level_cost[2] = 250;
    classroom.level_cost[3] = 350;
    classroom.level_cost[4] = 500;

    classroom.capacity[0] = 10;
    classroom.capacity[1] = 15;
    classroom.capacity[2] = 20;
    classroom.capacity[3] =  30;
    classroom.capacity[4] = 40;

    classroom.money_added[0] = -50;
    classroom.money_added[1] = -50;
    classroom.money_added[2] = -100;
    classroom.money_added[3] = -100;
    classroom.money_added[4] = -150;

    classroom.happiness_added = 0;

    cafe.level_cost[0] = 300;
    cafe.level_cost[1] = 400;
    cafe.level_cost[2] = 500;
    cafe.level_cost[3] = 650;
    cafe.level_cost[4] = 750;

    cafe.capacity[0,1,2,3,4,5] = 0;

    cafe.happiness_added = 50;
    cafe.money_added[0] = 50;
    cafe.money_added[1] = 70;
    cafe.money_added[2] = 100;
    cafe.money_added[3] = 125;
    cafe.money_added[4] = 150;

    library.level_cost[0] = 200;
    library.level_cost[1] = 300;
    library.level_cost[2] = 400;
    library.level_cost[3] = 500;
    library.level_cost[4] = 750;

    library.money_added[0] = 50;
    library.money_added[1] = 60;
    library.money_added[2] = 70;
    library.money_added[3] = 80;
    library.money_added[4] = 100;

    scienceLab.level_cost[0]=1000;
    scienceLab.level_cost[1]=1500;
    scienceLab.level_cost[2]=1500;
    scienceLab.level_cost[3]=2000;
    scienceLab.level_cost[4]=3000;

    scienceLab.money_added[0]=-50;
    scienceLab.money_added[1]=-70;
    scienceLab.money_added[2]=-100;
    scienceLab.money_added[3]=-140;
    scienceLab.money_added[4]=-200;

    sportsHall.level_cost[0]=500;
    sportsHall.level_cost[1]=700;
    sportsHall.level_cost[2]=900;
    sportsHall.level_cost[3]=1200;
    sportsHall.level_cost[4]=1500;

    sportsHall.happiness_added = 50;

    sportsHall.money_added[0] = -5;
    sportsHall.money_added[1] = -10;
    sportsHall.money_added[2] = -15;
    sportsHall.money_added[3] = -10;
    sportsHall.money_added[4] = -5;
}