#ifndef PARAMS_H
#define PARAMS_H

//Global variables that will be changed throughout the course of the program.
//Add all gameplay variables here.
extern const int MAX_NUMBER_OF_ROOMS;
extern const int MAX_NUMBER_OF_STUDENTS;
//-------------------------------------
extern int playerMoneyValue;
extern int playerStudentsValue;
extern int playerStudentCapacity;
extern int playerNumberRooms;
extern int playerStudentHappiness;
extern int playerPrestige;
extern int updateTracker;
//-------------------------------------
extern int moneyOverTime;
extern int studentsOverTime;
extern int happinessOverTime;
//-------------------------------------

#include "parameters.h"

int canRoombeAdded(int x, int y);
int canStudentBeAdded(int x, int y);
void flatAddMoney(int x);
void flatAddStudents(int x);
void flatIncreaseStudentCapacity(int x);
void flatAddHappiness(int x);
void flatRemoveMoney(int x);
void flatRemoveStudents(int x);
void flatDecreaseStudentCapacity(int x);
void flatRemoveHappiness(int x);
void addBuildingParam();
void removeBuildingParam();
void increaseMoneyOverTime(int x);
void decreaseMoneyOverTime(int x);
void increaseStudentsOverTime(int x);
void decreaseStudentsOverTime(int x);
void increaseHappinessOverTime(int x);
void decreaseHappinessOverTime(int x);
void updatePlayerParameters();

#endif //PARAMS_H