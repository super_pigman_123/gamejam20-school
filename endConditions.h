//Global variables that will be changed throughout the course of the program. 
//Add all gameplay variables here.
extern const int MAX_NUMBER_OF_ROOMS;
extern const int MAX_NUMBER_OF_STUDENTS;
//-------------------------------------
extern int playerMoneyValue;
extern int playerStudentsValue;
extern int playerStudentCapacity;
extern int playerNumberRooms;
extern int playerStudentHappiness;
extern int playerPrestige;
extern int updateTracker;
//-------------------------------------
extern int moneyOverTime;
extern int studentsOverTime;
extern int happinessOverTime;
//-------------------------------------
int checkWinState(int x){
    if (x >= 10) return 1;
    else return 0;
}

int checkLoseState(int x, int y, int z){
    int numLossFlags = 0;

    if (x <= 20 && y < 4) numLossFlags++;
    if (z < 350) numLossFlags++;
    if (numLossFlags == 0 || numLossFlags == 1) return 0;
    if (numLossFlags == 2) return 1;
    return 0;
}

int checkGameState(){
    if (checkWinState(playerPrestige) == 1)
        return 1;
    else if (checkLoseState(playerStudentsValue, playerPrestige, playerStudentHappiness) == 1)
        return -1;
    return 0;
} 