#ifndef MAIN_H
#define MAIN_H

//util
int BlockX_to_AbsX(int x);
int BlockY_to_AbsY(int y);
Rectangle makeRectangle (int x, int y, int w, int h);

//map logic
void init_map();

//rendering
int DrawRoomFromMap(int blockx, int blocky);
void DrawFullRoomMap();
int DrawKidsInRooms();

//click processing
void processMapClicks();
void initRoomRectangles();
struct room * getRoomPointedByMouse(Vector2 mouse_pos);

#endif //MAIN_H
